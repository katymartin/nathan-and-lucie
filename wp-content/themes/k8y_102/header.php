


	<!doctype html>

	<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
	<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
	<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
	<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



		<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
	  <meta http-equiv="content-language" content="nl">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
	<link href="/wp-content/themes/k8y_102/library/css/aos.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Raleway:400,800" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

	<link rel="canonical" href="http://nathanlucie.com" />
	        <meta property="og:url" content="http://nathanlucie.com"/>
	        <meta property="og:title" content="Nathan and Lucie Wedding Site" />
	        <meta property="og:description" content="Get all the info for our big day!" />
	        <meta property="og:type" content="article" />
	        <meta property="og:image" content="http://nathanlucie.com/wp-content/themes/k8y_102/library/images/NathanLucie_FBpreview.jpg" />
	        <meta property="og:image:width" content="1200" />
	        <meta property="og:image:height" content="628" />





			<title><?php wp_title(''); ?></title>

			<?php wp_head(); ?>




		</head>

		<body id="wrapper">
