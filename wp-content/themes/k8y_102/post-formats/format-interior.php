

			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
			<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
				<a href="<?php the_permalink(); ?>"><img alt="<?php the_field('play_alt'); ?>" class="logoPrev" src="<?php the_field('play_button'); ?>"></a>
				<div class="aInfo">
					<h2><?php the_title(); ?></h2>
					<p><?php the_excerpt(); ?> <a href="<?php the_permalink(); ?>"><?php get_field('read_m'); ?></a></p>
				</div>
			</div>
